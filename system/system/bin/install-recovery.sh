#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:bf7709c063622e512d2c5d8ec25a0e5f695a1a43; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:c71425e56acc45f1c722656a977de0eb63fc8a15 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:bf7709c063622e512d2c5d8ec25a0e5f695a1a43 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
